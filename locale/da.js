export default {
  barTitle: 'Cookies',
  barDescription: 'Vi bruger vores egne cookies og tredjepartscookies, så vi kan vise dig dette websted og bedre forstå hvordan du bruger det, med henblik på at forbedre de tjenester, vi tilbyder. Hvis du fortsætter med at se på siden accepterer du cookies.',
  acceptAll: 'Accepter alle',
  declineAll: 'Slet alle',
  manageCookies: 'Administrer cookies',
  unsaved: 'Du har ikke gemt indstillinger',
  close: 'Luk',
  save: 'Gem',
  necessary: 'Nødvendige cookies',
  optional: 'Valgfri cookies',
  functional: 'Funktionelle cookies',
  blockedIframe: 'For at se dette skal du aktivere funktionelle cookies',
  here: 'her'
}
