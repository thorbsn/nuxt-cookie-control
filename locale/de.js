export default {
  barTitle: 'Cookies',
  barDescription: 'Wir verwenden unsere eigenen Cookies und Cookies von Drittanbietern damit wir Ihnen diese Website zeigen können, und verstehen wie Sie diese verwenden, um die von uns angebotenen Dienstleistungen zu verbessern. Wenn Sie weiter surfen, gehen wir davon aus, dass Sie die Cookies akzeptiert haben.',
  acceptAll: 'Alles akzeptieren',
  declineAll: 'Alles löschen',
  manageCookies: 'Cookies verwalten',
  unsaved: 'Sie haben nicht gespeicherte Einstellungen',
  close: 'Close',
  save: 'Save',
  necessary: 'Notwendige cookies',
  optional: 'Optionale cookies',
  functional: 'Funktionale cookies',
  blockedIframe: 'Um dies zu sehen, aktivieren Sie bitte funktionierende Cookies',
  here: 'hier'
}